const path = require("path");

module.exports = {
  entry: "./src/index.js",
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              [
                "env",
                {
                  targets: {
                    browers: ["last 2 versions"]
                  }
                }
              ],
              "react"
            ],
            plugins: ["transform-class-properties", "transform-object-rest-spread"]
          }
        },
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [".js", ".jsx"]
  },
  externals: {
    "styled-components": "commonjs styled-components"
  },
  output: {
    filename: "index.js",
    library: "core",
    libraryTarget: "umd",
    path: path.resolve(__dirname, "dist")
  }
};
