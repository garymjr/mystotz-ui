import React from "react";
import PropTypes from "prop-types";

export class Tab extends React.Component {
  static propTypes = {
    active: PropTypes.bool,
    component: PropTypes.element.isRequired,
    static: PropTypes.bool,
    tabId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    title: PropTypes.string.isRequired
  };

  render() {
    const { component } = this.props;

    return React.cloneElement(component, { ...this.props });
  }
}
