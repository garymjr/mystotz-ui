import React from "react";
import PropTypes from "prop-types";

export class Select extends React.Component {
  static propTypes = {
    options: PropTypes.array,
    value: PropTypes.string,
    onChange: PropTypes.func
  };

  render() {
    const { options = [], value = "", onChange = null } = this.props;

    return (
      <select className="form-control" value={value} onChange={onChange}>
        {options.map((option, index) => (
          <option key={index} value={option.value}>{option.name}</option>
        ))}
      </select>
    );
  }
}
