import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

class SelectMultiple extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    onSelect: PropTypes.func.isRequired,
    onSelectAll: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
    placeholder: PropTypes.string,
    selected: PropTypes.array.isRequired
  };

  state = { open: false };

  render() {
    const { className, options, selected, placeholder = "Select an Item" } = this.props;

    return (
      <div className={className} onMouseEnter={this._enterMenu.bind(this)} onMouseLeave={this._leaveMenu.bind(this)}>
        <div className="dropdown">
          <button className="btn btn-default dropdown-toggle" type="button" onClick={this._toggle.bind(this)}>
            {selected.length > 0 ? `${selected.length} item(s) selected` : placeholder} <span className="caret" />
          </button>
          <ul className={`dropdown-menu${this.state.open ? " show" : ""}`}>
            <li>
              <a href="#" onClick={event => this._selectAll(event)}>
                <input type="checkbox" checked={options.length === selected.length} readOnly />{" "}
                {options.length === selected.length ? "Uncheck" : "Check"} All
              </a>
            </li>
            {options.map((option, index) => (
              <li key={index}>
                <a href="#" onClick={event => this._select(event, option.value)}>
                  <input type="checkbox" checked={selected.indexOf(option.value) !== -1} readOnly /> {option.text}
                </a>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }

  _toggle(event, open = null) {
    if (open !== null) {
      this.setState({ open });
    } else {
      this.setState({ open: !this.state.open });
    }
  }

  _select(event, value) {
    event.preventDefault();
    this.props.onSelect(value);
  }

  _selectAll(event) {
    event.preventDefault();
    this.props.onSelectAll();
  }

  _leaveMenu() {
    this.timer = setTimeout(this._toggle.bind(this), 100, null, false);
  }

  _enterMenu() {
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
  }
}

export const SelectMultipleStyled = styled(SelectMultiple)`
  .dropdown,
  .dropdown-toggle {
    width: 100%;
  }

  .dropdown-menu {
    display: none;
    width: 100%;
  }

  .dropdown-menu.show {
    display: block;
  }
`;
