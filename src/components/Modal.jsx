import React, { Fragment } from "react";
import PropTypes from "prop-types";

class Modal extends React.Component {
  static propTypes = {
    component: PropTypes.element.isRequired,
    visible: PropTypes.bool.isRequired,
    onOk: PropTypes.func,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    size: PropTypes.string,
    title: PropTypes.string,
    className: PropTypes.any
  };

  render() {
    const { visible, size, component, title = "" } = this.props;
    const modalIn = "modal fade in";
    const modalOut = "modal fade";
    const sizeClass = ["modal-dialog"];
    if (size === "sm") {
      sizeClass.push("modal-sm");
    } else if (size === "lg") {
      sizeClass.push("modal-lg");
    }

    if (visible) {
      this._handleOpen();
    }

    return (
      <Fragment>
        <div
          className={`${visible ? modalIn : modalOut}`}
          role="dialog"
          tabIndex="-1"
          style={visible ? { display: "block", paddingLeft: "0px" } : {}}
        >
          <div className={sizeClass.join(" ")} role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" onClick={this._handleClose.bind(this)}>
                  &times;
                </button>
                <h4 className="modal-title">{title}</h4>
              </div>
              <div className="modal-body">{component}</div>
              <div className="modal-footer">
                <button
                  type="button"
                  className={`btn btn-${this.props.onOk ? "danger" : "primary"}`}
                  onClick={this._handleClose.bind(this)}
                >
                  Close
                </button>
                {this.props.onOk ? (
                  <button type="button" className="btn btn-primary" onClick={this._handleOk.bind(this)}>
                    Save Changes
                  </button>
                ) : null}
              </div>
            </div>
          </div>
        </div>
        {visible ? <div className="modal-backdrop fade in" /> : null}
      </Fragment>
    );
  }

  _handleOpen() {
    const body = document.querySelector("body");
    let bodyClass = body.className.split(" ");
    if (this.props.visible && bodyClass.indexOf("modal-open") === -1) {
      body.className = `modal-open ${bodyClass.join(" ")}`;
    }

    if (this.props.onOpen) {
      this.props.onOpen();
    }
  }

  _handleClose() {
    const body = document.querySelector("body");
    let bodyClass = body.className.split(" ");
    if (bodyClass.indexOf("modal-open") >= 0) {
      bodyClass = bodyClass.filter(item => item !== "modal-open");
      body.className = bodyClass.join(" ");
    }

    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  _handleOk() {
    if (this.props.onOk) {
      this.props.onOk();
    }

    this._handleClose();
  }
}

export default Modal;
