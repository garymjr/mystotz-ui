import React from "react";
import PropTypes from "prop-types";

export class Col extends React.Component {
  static propTypes = {
    children: PropTypes.any,
    xs: PropTypes.number,
    sm: PropTypes.number,
    md: PropTypes.number,
    lg: PropTypes.number,
    xsOffset: PropTypes.number,
    smOffset: PropTypes.number,
    mdOffset: PropTypes.number,
    lgOffset: PropTypes.number,
    hiddenXs: PropTypes.bool,
    hiddenSm: PropTypes.bool,
    hiddenMd: PropTypes.bool,
    hiddenLg: PropTypes.bool
  };

  render() {
    const className = this._createClassName();
    return <div className={className}>{this.props.children}</div>;
  }

  _createClassName() {
    const className = [];
    ["xs", "sm", "md", "lg"].forEach(size => {
      if (this.props[size]) {
        className.push(`col-${size}-${this.props[size]}`);
      }
    });

    ["xsOffset", "smOffset", "mdOffset", "lgOffset"].forEach(size => {
      if (this.props[size]) {
        const offset = size
          .split(/(?=[A-Z])/)
          .join("-")
          .toLowerCase();
        className.push(`col-${offset}-${this.props[size]}`);
      }
    });

    ["hiddenXs", "hiddenSm", "hiddenMd", "hiddenLg"].forEach(size => {
      if (this.props[size]) {
        const hidden = size
          .split(/(?=[A-Z])/)
          .join("-")
          .toLowerCase();
        className.push(hidden);
      }
    });

    return className.join(" ");
  }
}
