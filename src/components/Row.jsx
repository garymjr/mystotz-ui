import React from "react";

export class Row extends React.Component {
  render() {
    const className = ["row"];
    if (this.props.className) {
      this.props.className.split(" ").forEach(name => {
        className.push(name);
      });
    }

    return <div className={className.join(" ")}>{this.props.children}</div>;
  }
}
