import React from "react";

class Container extends React.Component {
  render() {
    const { fluid = true } = this.props;

    if (!this.props.fluid) {
      return <div className="container">{this.props.children}</div>;
    }
    return <div className="container-fluid">{this.props.children}</div>;
  }
}

export default Container;
