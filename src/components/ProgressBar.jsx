import React from "react";
import PropTypes from "prop-types";

export class ProgressBar extends React.Component {
  static propTypes = {
    data: PropTypes.array,
    onFinish: PropTypes.func
  };

  intervalId = null;

  state = { timer: 1 };

  componentDidMount() {
    this.intervalId = setInterval(() => this._increment(), 300);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  render() {
    // If we're no longer loading display nothing
    if (this.state.timer === 100) {
      return null;
    }

    return (
      <div className="progress" style={{ width: "500px", margin: "0 auto" }}>
        <div
          className="progress-bar progress-bar-success progress-bar-striped"
          role="progressbar"
          aria-valuenow={this.state.timer}
          aria-valuemin="0"
          aria-valuemax="100"
          style={{ width: `${this.state.timer}%` }}
        >
          <span className="sr-only">{this.state.timer}% Complete (success)</span>
        </div>
      </div>
    );
  }

  _increment() {
    const { data = [] } = this.props;

    if (this.state.timer >= 100) {
      clearInterval(this.intervalId);
      this._handleFinish();
      return;
    }

    if (data.length === 0) {
      this.setState({ timer: this.state.timer + 1 });
    }

    if (data.length > 0 && this.state.timer < 90) {
      this.setState({ timer: 90 });
    } else if (data.length > 0) {
      this.setState({ timer: this.state.timer + 10 });
    }
  }

  _handleFinish() {
    if (this.props.onFinish) {
      this.props.onFinish();
    }
  }
}
