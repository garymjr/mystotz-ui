import React from "react";
import PropTypes from "prop-types";

export class Button extends React.Component {
  static propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    type: PropTypes.oneOf(["primary", "success", "warning", "danger"]),
    onClick: PropTypes.func,
    size: PropTypes.string,
    fill: PropTypes.bool
  };

  render() {
    const { size = "sm", type = "primary", onClick = null, value } = this.props;

    const style = {};
    if (this.props.fill) {
      style.width = "100%";
    }

    return (
      <button type="button" className={`btn btn-${size} btn-${type}`} style={style} onClick={onClick}>
        {value}
      </button>
    );
  }
}
