import React, { Fragment } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

// type Tab {
//   title: string;
//   component: React.Component
// }

class Tabs extends React.Component {
  static propTypes = {
    activeTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    children: PropTypes.any,
    className: PropTypes.string,
    onClickTab: PropTypes.func,
    onCloseTab: PropTypes.func,
    shownTabs: PropTypes.number
  };

  render() {
    const { children, shownTabs = 10 } = this.props;

    let tabs = [];
    React.Children.forEach(children, child => {
      if (React.isValidElement(child)) {
        tabs.push(child);
      } else {
        tabs = tabs.concat(child || []);
      }
    });

    let staticTabs = tabs.filter(tab => tab.props.static);
    let fluidTabs = tabs.filter(tab => !tab.props.static);

    const tabsArr = staticTabs.concat(fluidTabs.slice(staticTabs.length - shownTabs));

    return (
      <Fragment>
        <ul className={`nav nav-tabs ${this.props.className}`} role="tablist">
          {tabsArr.map((tab, index) => {
            index = tab.props.tabId ? tab.props.tabId : index;
            return (
              <li key={index} role="presentation" className={this._isActiveTab(index) ? "active" : ""}>
                <a href="#" role="tab" onClick={this._onClickTab.bind(this, index)}>
                  {tab.props.title}
                </a>
                {!tab.props.static ? (
                  <i
                    className="glyphicon glyphicon-remove remove-button"
                    onClick={this._onCloseTab.bind(this, index)}
                  />
                ) : null}
              </li>
            );
          })}
        </ul>
        <div className="tab-content">
          {tabsArr.map((tab, index) => {
            index = tab.props.tabId ? tab.props.tabId : index;
            const className = ["tab-pane"];
            if (this._isActiveTab(index)) {
              className.push("active");
            }

            return (
              <div key={index} role="tabpane" className={className.join(" ")}>
                {tab}
              </div>
            );
          })}
        </div>
      </Fragment>
    );
  }

  _isActiveTab(tabId) {
    return this.props.activeTab === tabId;
  }

  _onClickTab(tabId, event) {
    event.preventDefault();
    if (this.props.onClickTab) {
      this.props.onClickTab(tabId);
    }
    return null;
  }

  _onCloseTab(tabId, event) {
    event.preventDefault();
    if (this.props.onCloseTab) {
      this.props.onCloseTab(tabId);
    }
    return null;
  }
}

const TabsStyled = styled(Tabs)`
  display: flex;
  flex-flow: row nowrap;
  margin-bottom: 5px;
  max-width: 100%;

  > li {
    a {
      text-align: center;
      text-overflow: ellipsis;
      overflow: hidden
      padding-right: 20px;
    }

    flex: 1 1 auto;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden
  }

  .remove-button {
    opacity: 0.5;
    position: absolute;
    top: 14px;
    right: 10px;
  }

  .remove-button:hover {
    opacity: 1;
    cursor: pointer;
  }
`;

export default TabsStyled;
